---
title: infographic – 2.A Gold
icon: ti-hummer
description: Graphic showing the lifecycle of a mineral
type: docs
weight: 31
downloadBtn: true
# search keywords
keywords: ["lifecycle", "mineral"]

---

![gold](https://www.sankey-diagrams.com/wp-content/gallery/x_sankey_217/Gold-Market-2013-Sankey.png)