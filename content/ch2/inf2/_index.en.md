---
title: infographic – 2.B LCA
icon: ti-hummer
description: 1 page ‘flowchart’ style illustration lifecycle of one mineral and its social and environmental impacts from exploration to disposal – and existing circular solutions across the full lifecycle of minerals – product design, remanufacturing, repair, recycling
type: docs
weight: 31
downloadBtn: true
# search keywords
keywords: ["lifecycle", "mineral", "social and environmental impacts", "exploration", "disposal", "circular", "product design", "remanufacturing", "repair", "recycling"]

---

![LCA](https://i.pinimg.com/736x/62/7e/e3/627ee377c507a7d41facdc62957dc59e.jpg)