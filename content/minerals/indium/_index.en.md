---
title: K. Indium
description: Critical Minerals in detail  K. Indium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "K. Indium", "indium"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.020.png)
Analysis by UNEP. All rights reserved.
## **2. Geographical Data**
![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.021.jpeg)

Data source: USGS 2022, p. 81. Analysis by UNEP. All rights reserved.
## **3.Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Indium<br>(<sup>49</sup> In)</b>|<p>The name of the metal, indium, is based on its bright indigo line spectrum. It is a soft and silvery metal stable in air and water at standard temperature and pressure. The by-products of zinc refineries and Zinc minerals have been used as the primary mineral sources for its commercial production (Pradhan, Panda and Sukla 2018, p. 167). </p><p>Indium is a relatively rare metal, whose economic value in various high-tech industries is high. It is used in liquid-crystal displays (LCD) for computers, TV sets and mobile devices, and solar batteries. About 10 indium minerals are now known, but the main economically significant Indium bearing mineral is sphalerite. Indium is obtained as a by-product in Zinc, Tin and Copper production. Many countries such as the USA and North European countries import indium (Ivashchenko 2022, p. 2).</sup> </p><p>Sphalerite (average indium content is 1927 ppm) is a major indium bearing mineral found in the Pitkaranta Mining District in Northeastern Russia skarn deposits. Smaller quantities of indium are present in chalcopyrite, pyrite, stannoidite, wittichenite, arsenopyrite, loellingite, magnetite and fluorite Ivashchenko 2022, p. 16).</p>||
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>**In 2013, an indium ore-dressing plant in the city of Hezhou in the Guangxi Province, China is reported to have used illegal indium production processes that resulted in excessive thallium and cadmium pollution in the Hejiang River (Duan *et al*. 2016, p. 91).** </p><p>**A significant fraction of tailings containing Indium could be leached into freshwaters, especially under the acidic conditions of mine drainage. Waters around the ASARCO copper smelter and refinery in Tacoma, Washington, contained up to 500 μg/L indium, and tailings decant outfalls near the Bunker Hill Company in Kellogg, Idaho (which produced Lead, Zinc, Copper, and Silver), contained up to 200 μg/L indium (White *et al.* 2017, p. 1485).**</p><p>The heavy metal content of tailings and slags is also often a cause for environmental concern, and hence most are considered wastes. If the extraction from these sources leads to net reductions in local pollutant levels and/or the offset of demand from primary production, the extraction of resources held by tailings and slags can be environmentally attractive (Werner, Mudd and Jowitt 2017, p. 942).<a name="_ref123139022"></a> </p>|<p>Worldwide, many researchers are exploring the experiments on different hydrometallurgical processes, including chemical leaching, bioleaching, solvent extraction, electrometallurgy, adsorption and wet precipitation for metal recovery from ores, concentrates and wastes (Pradhan, Panda and Sukla 2018, p. 170).</p><p>Since Indium is mainly produced and used in the form of indium-tin oxide, numerous cases occurred in electronic products manufacturing involving Indium containing materials. Inhaled indium-tin-oxide particles tend to accumulate in the deep parts of lung and then induce pulmonary alveolar proteinosis, fibrosis and emphysema, even malignant lung tumors (Zhang *et al.* 2021, p. 2).</p><p>Given the huge yield and consumption of coal and its potential environmental menace, the enrichment of Gallium and Indium in coal should be taken into account in respect of their risks on environment and human health as well. Accumulation of Indium in lung tissues could induce various diseases, including pulmonary alvoelar proteinosis, pneumonia, and lung cancer (Zhang *et al.* 2021, p. 14).</p>|<p>Mineral resources of indium in Australia are estimated at ∼46 213 t, among 247 mineral deposits. Such quantities, if extracted and refined, would be sufficient on their own to meet global demand for many decades, although at present the infrastructure and economic drive within the Australian mining industry does not exist to support such activities (Werner *et al.* 2018, p. 2058).</p><p>LCD and LED screens for TVs, laptops, tablets, and desktop computers account for the majority of imported indium to Australia (at least since 1995) (</p><p></p>|
## **5. Analysis**
Indium is one of the least abundant elements on Earth and has a supply chain that is dominated by relatively few countries. This has led to its classification by multiple government and industry bodies as a critical metal (Werner, Mudd and Jowitt 2017, p. 939). Indium is an increasingly important metal in semiconductors and electronics. No regulations for drinking water or the protection of aquatic life presently exist for indium (White *et al.* 2017, p. 1485).

Producing primary indium from other ores such as sulfidic lead, tin, copper and iron as well as past refining residues may become viable as shortage of indium drives up prices of the commodity (Chai, Cao and Zhao 2016, p. 69). Recycling of indium from waste LCDs are in progress to combat Indium resource shortages (Pradhan, Panda and Sukla 2018, p. 167).The annual production of indium from mining is 480t; however, that from recycling is 650t (Pradhan, Panda and Sukla 2018, p. 177).

A number of studies have quantified factors to establish ratings of metal criticality, generally agreeing that the specialty metals used in modern technologies such as indium, germanium, gallium, antimony, and the rare earth elements are typically the most critical. Indium remains inefficiently produced and recycled. Given its primary uses in liquid-crystal display (LCD) and light-emitting diode (LED) displays and solar panels, it is projected that indium demand (currently ∼800 t/year) will continue to increase for the foreseeable future. Recent estimates of the global mineral resources of indium show that mineral resources are not a limiting factor for demand this century (Werner *et al.* 2018, p. 2055).

Almost 90% of indium is used in electronics, a sector with a quick development. Currently, indium is extracted from ores where it is present in concentrations of about 1–100 ppm. China is the largest producer of indium, followed by Korea, Japan and Canada, with an estimated world refinery production in 2013 of 770 metric tons. The European Commission has classified indium as a critical raw material for Europe for its high supply risk and high economic importance (Rocchetti *et al.* 2015, p. 180).

The Indium export trade used to be dominated by China. Japan and Korea mainly produce secondary indium and have gradually surpassed China to become the major exporters. It therefore implies that China has transitioned from primarily exporting indium to primarily consuming indium domestically (Duan *et al*. 2016, p. 91).

