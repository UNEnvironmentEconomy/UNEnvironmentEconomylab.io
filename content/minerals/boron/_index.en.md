---
title: B. Boron
description: Critical Minerals in detail for B. Boron
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "B. Boron", "boron"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.003.png)

Analysis by UNEP. All rights reserved.

## **2. Geographical Data**

Insufficient data means that it is not possible to portray world production and reserves in visual format. The USGS 2021 figures combine production in all forms (crude ore, ulexite, boric oxide equivalent, compounds, crude compounds, datolite ore and refined borates) from Argentina, Bolivia, Chile, China, Germany, Peru, Russia and Turkey, but explicitly withhold both US production figures and the world total for the year, so as to preclude the disclosure of company proprietary data (USGS 2022, p. 39). Similarly, world reserve totals “cannot be calculated because production and reserves are not reported in a consistent manner by all countries.” (USGS 2022, p. 39). However, it is known that the supply of boron is controlled primarily by two producers: the Turkish state-owned ETI Maden accounts for approximately 50% of the global share of boron, and U.S. Borax of America accounts for approximately 30% of the global market share (Elevli, Yaman and Laratte 2022, p. 156). Four sectors account for approximately 86% of the world’s boron consumption – glass, ceramics, detergents and fertilizer (Elevli, Yaman and Laratte 2022, p. 157).

## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Boron<br>(<sup>5</sup> B)</b>|Boron does not occur in nature in an elemental state. It combines with oxygen and other elements to create boric acid or inorganic salts named borates. Boron products are bought and sold based on their boric oxide (B<sub>2</sub>O<sub>3</sub>) content, which varies by ore and compound and by the absence or presence of calcium and sodium (USGS 2022, p. 39).|Boron refineries follow different methodologies, dependent on the boron compound in question (USGS 2022, p. 39).|

## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Around an area featuring prominent Turkish boron mines, the groundwater levels of boron and arsenic were elevated and exceeded the tolerance level (Gemici et al 2008, pp. 2742-2773).</p><p>Anthropogenic sources of boron, including from mining waste can lead to elevated levels of soil boron concentration, affecting plant life (Kot 2009 at 3).</p><p></p>|<p>Severe boron contamination of agriculture soils contributes to reduced crop yields (Yau and Ryan 2008). </p><p>Animal studies have indicated that the developing fetus and testes as the two most sensitive targets of boron toxicity. Testes have been reported as atrophying and degeneration of the spermatogenic epithelium (Kot 2009). The developmental effects that have been noted include high prenatal mortality, reduced fetal body weight and variations of the eyes, central nervous system, cardiovascular system and axial system (Kot 2009). However, other studies have not found such similar effects in humans (Duydu 2011; Duydu 2018).</p>||

## **5. Analysis**
Given that deposits of borates are associated with volcanic activity and arid climates,, the largest economically viable deposits are located “in the Mojave Desert of the United States, the Alpide belt along the southern margin of Eurasia, and the Andean belt of South America.” (USGS 2022, p. 39). The USGS deems world resources to be “adequate for the foreseeable future” at current consumption levels (USGS 2022, p. 39). However, given that the supply of boron is predominantly controlled by just two countries (Turkey and the United States), this statement may not necessarily provide comfort to other countries that could experience supply chain tightness pursuant to geopolitical tensions.

Recycling and substitution of boron resources may be a way to make the extraction of primary boron sustainable. Most promising in terms of recycling is the recycling of glass fibers from glass fiber reinforced plastics. The substitution of glass wool with other insulating materials is the most promising substitution of born materials (Henckens, Driessen and Worrel 2015, p. 17).