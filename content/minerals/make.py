import os

MINERALS = [
    'A. Aluminium',
    'B. Boron',
    'C. Cadmium',
    'D. Chromium',
    'E. Cobalt',
    'F. Copper',
    'G. Gallium',
    'H. Germanium',
    'I. Gold',
    'J. Graphite',
    'K. Indium',
    'L. Lead',
    'M. Lithium',
    'N. Manganese',
    'O. Molybdenum',
    'P. Nickel',
    'Q. Platinum Group Minerals [PGMs]',
    'R. Rare Earth Elements [REEs]',
    'S. Silicon',
    'T. Silver',
    'U. Tellurium',
    'V. Tin',
    'W. Titanium',
    'X. Vanadium',
    'Y. Zinc',
    'Z. Zirconium',  
]

HEAD = """
---
title: {0:s}
description: {0:s}
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "{0:s}", "{1:s}"]

---
"""

def make_page(mineral):
    folder = mineral\
        .split('.')[-1]\
        .strip()\
        .lower()\
        .replace(' ', '_')\
        .replace('_[pgms]', '')\
        .replace('_[rees]', '')
    print(mineral)
    if not os.path.isdir(folder):
        os.mkdir(folder)
    else:
        print(folder)

    head = HEAD.format(mineral, folder)
    with open("{}/_index.en.md".format(folder), 'w') as doc:
        doc.write(head)


def main():
    for mineral in MINERALS:
        make_page(mineral)

if __name__ == "__main__":
    main()
