
---
title: Z. Zirconium
description: Critical Minerals in detail  Z. Zirconium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "Z. Zirconium", "zirconium"]

---

## **1. Treatment by Critical Minerals Lists**

![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.049.png)
Analysis by UNEP. All rights reserved.

## **2. Geographical Data**

![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.050.jpeg)

Data source: USGS 2022, pp. 18, 195. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Zirconium <br>(<sup>40</sup> Zr)</b>|Surface mining – placer deposits in heavy mineral sands. Possible offshore potential. By- product of open pit mining of ilmenite and rutile (Great Mining n.d.).|Heavy mineral sands are processed to separate each heavy mineral by weight and magnetism.|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Erosion and stripping of land due to surface mining, and the disruption of the surrounding ecosystem (especially costal ecosystems, where most mineral sand deposits are located) (Perks 2022).</p><p>Mining process requires a lot of energy, and results in the use of diesel fueled machines (Perks 2022).</p><p>Waste by-product of zirconium mining has relatively high radioactivity (Perks 2022).</p><p>Ecological health concerns from zirconium are minimal in the aquatic environment and in soils because of its low solubility. Only a small proportion of the zirconium in soil is taken up by plants, and it does not accumulate in the food chain (Jones, Piatak and Bedinger n.d., p. V17).</p>|<p>Inhalation of zirconium dust during industrial processing.</p><p>Although an unlikely occurrence, the ingestion of large quantities of zirconium can cause health issues in humans (Jones, Piatak and Bedinger n.d., p. V17).</p><p>Zirconium may cause dermal lesions upon exposure (Jones, Piatak and Bedinger n.d., p. V17).</p><p>Inhalation of zirconium metals or compounds during processing has led to reports of respiratory tract irritation, dermatitis, pulmonary fibrosis, and one instance of death occasioned by an acute hypersensitivity reaction (Jones, Piatak and Bedinger n.d., p. V17).</p><p>Tailings dam failures have taken place at heavy-mineral sands dams, such as the Mozambique Moma heavy-mineral sands mine whose settling pond breached its retaining wall, flooding a nearby village and causing one death and extensive property damage (Jones, Piatak and Bedinger n.d., p. V18).</p>|Regulation of radiation exposure may be an issue (Harlow 2017). |

## **5. Analysis**
71 % of the world’s known reserves are located in a single country (Australia) and three countries jointly account for 68% of global mining production (Australia – 33%; South Africa – 23%; and China – 12%). World zircon reserves are estimated to be 78 million metric tons of contained zirconium oxide (ZrO<sub>2</sub>), and estimated resources include about 14 million metric tons of zircon associated with titanium resources in heavy-mineral sand deposits (Jones, Piatak and Bedinger n.d., p. V12). Because global zircon production has historically been a byproduct of titanium mining, potentially significant concentrations of zircon might be present in tailings that were exclusively mined for titanium, or for other commodities like gold, iron, REEs or tin (Jones, Piatak and Bedinger n.d., p. V13). This means that circularity could potentially play an important role when it comes to addressing any zirconium shortages, through the mining of tailings to extract value from waste. According to a 2017 USGS Report, less than 1% of discarded zirconium is recycled or reused worldwide, with the largest proportion of recycled zirconium hailing from scrap generated during metal production and manufacturing; the recycled zirconium content in products globally ranged from 1-10% (Jones, Piatak and Bedinger n.d., p. V14).[^2] This was ascribed to three factors: (1) the complex materials in which it is used, such as electronic equipment; (2) the technical difficulty of recycling them; and (3) the fact that such minute amounts are used in these products (Jones, Piatak and Bedinger n.d., p. V14). The USGS considers the future outlook for zirconium to be “generally favorable” given the relative abundance of zircon in crustal rocks (Jones, Piatak and Bedinger n.d., p. V18).

[^1]: 
[^2]: Jones, “Zirconium and Hafnium” *supra* note **Error! Bookmark not defined.** at V14.


