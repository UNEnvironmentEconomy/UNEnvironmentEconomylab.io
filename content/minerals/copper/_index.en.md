---
title: F. Copper
description: Critical Minerals in detail  F. Copper
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "F. Copper", "copper"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.010.png)

Analysis by UNEP. All rights reserved.
## **2. Geographical Data**
![Chart, application, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.011.jpeg)

Data source: USGS 2022, p. 55. Analysis by UNEP. All rights reserved.

## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Copper<br>(<sup>29</sup> Cu)</b>|Extracted from sulphide and oxide ores by both open pit and underground methods (*Mining Methods* n.d.).|Oxide ores are treated by acid dissolution and electrowinning, while sulphide ores are processed by flotation and smelting (Superfund Research Center n.d.).|

## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Water usage can be as high as 265,000 litres per ton of copper sulphide ore processed (Mussey 1961). The majority of copper mining is carried out in arid conditions. Smelting creates Sulfur Dioxide (Mussey 1961).</p><p>Power systems that utilize copper generate, transmit, and utilize energy with greater efficiency. Because of this, greenhouse gas emission and life cycle costs are decreased (Ulnikovic, Kurilic and Staletovic 2020, p. 3).</p><p>Copper can be recycled repeatedly without any loss of performance, and recycling requires up to 85% less energy than primary production. Globally, this saves 40 million tons of Carbon Dioxide annually (Ulnikovic, Kurilic and Staletovic 2020, p. 2).</p><p>**The environmental impacts of Cu mining are closely related to the grade (i.e. the copper concentration) of ore being mined. As ore grades decline, a larger amount of material has to be moved and processed to achieve the same unit of produced metal (Northey *et al.* 2014, p. 190).**</p><p>**The combination of increased mine size and declining ore grade results in increases in the rate of waste rock removal, tailings generation and area of local habitat disturbance. Declining ore grades also have impacts on other areas of environmental concern, such as increases in diesel and explosives consumption within the mine itself and also the water and energy consumption within ore processing facilities (Northey *et al.* 2014, p. 190).**</p><p>**There are sufficient Cu resources contained in currently identified deposits to sustain the current global average ore grade until at least 2030 if higher grade deposits are mined preferentially (Northey *et al.* 2014, p. 200).**</p><p>**The critical issues for primary Copper production are economic and environmental issues relating to energy consumption, water consumption and GHG emissions are likely to play much larger roles in restricting future production (Northey *et al.* 2014, p. 200).**</p>|<p>The particularly high variability of unemployment for instance in Antofagasta, the most important copper mining region in Chile, indicates that it suffers from such high level of exposure to commodity cycles and that this impacts the city in socio-economic terms (Rehner and Rodríguez 2021, p. 2).</p><p>Child labour, forced labour, corruption and human rights abuse (Michaels, Maréchal and Katz 2022).</p><p>Long-term exposure to copper from an environmental contaminant can cause irritation of the nose, mouth and eyes and causes headaches, stomach aches, dizziness, vomiting and diarrhoea. Intentionally, high uptakes of copper may cause liver and kidney damage or even death (Obiri *et al.* 2016, p. 16590).</p><p>Chronic copper poisoning result in Wilson’s disease, characterised by hepatic cirrhosis, brain damage, demyelination, renal disease, and copper deposition in the cornea (Obiri *et al.* 2016, p. 16590).</p><p>Free copper levels in the human brain as low was 0.12 kg<sup>-1</sup> dw were found to impede the efflux of amyloid from the brain, thus promoting formation of amyloid plaques, one of the hallmarks of Alzheimer’s disease (Filimon *et al.* 2021, p. 13).</p><p>Copper may be a metal that causes reproductive alterations (Yee-Duarte *et al.* 2020, p. 1).</p>|<p>A 2014 survey of mine workers in Zambia asked whether they would see national ownership of mining assets as beneficial. Of the 190 workers who responded, 180 hoped to see national ownership. Of 170 who specified reasons for their choice, 89 linked nationalization with improved working conditions and 81 focused on the benefit of nationalization for national development (Sautman and Hairong 2018, p. 57).</p><p>Corruption, non-state armed groups, abuse of force of security forces and non-payment of taxes (Michaels, Maréchal and Katz 2022).</p>|

## **5. Analysis**
As an element of modern materials, copper plays a vital role in global information, energy, and electronics, which has led to an incredible increase in copper production and expanding its market (Ulnikovic, Kurilic and Staletovic 2020, p. 3).

Refined copper production in China increased annually and reached 8.43 million tons in 2016. Furthermore, China consumed more than 40% of the world’s refined copper. Seven of the 20 largest copper smelters in the world were located in China (Ulnikovic, Kurilic and Staletovic 2020, p. 2).

A 2015 USGS study puts discovered global copper resources at 2.1 billion tons and estimates undiscovered resources at 3.5 billion tons (USGS 2022, p. 55).

