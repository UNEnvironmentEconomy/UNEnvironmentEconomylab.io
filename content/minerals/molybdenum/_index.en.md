---
title: O. Molybdenum
description: Critical Minerals in detail  O. Molybdenum
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "O. Molybdenum", "molybdenum"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.028.png)

Analysis by UNEP. All rights reserved.
## **2. Geographical Data**

![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.029.jpeg)

Data source: USGS 2022, p. 113. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Molybdenum<br>(<sup>42</sup> Mo)</b>|Mined by open pit and underground methods. Obtained as by-product of copper mining. (International Molybdenum Association [IMOA] n.d.).|Ore is separated by flotation, followed by leaching and roasting (IMOA n.d.).|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p><b>SO<sub>2</sub> is produced in roasting process.</b></p><p>**Soil and plant life heavy metal contamination surrounding molybdenum-copper mine (Timofeev *et al.* 2016; Yin *et al.* 2020).**</p><p>**Groundwater polluted in the mining region, mainly affected by tailings reservoir leakage, spoil bank leachate and infiltration of Cuo-luo River (Miao *et al.* 2019).**</p><p>**Heavy metal pollution in area near molybdenum mine exceed risk control standard for contamination of agricultural lands (Hui *et al.* 2021).**</p>|<p>Tungsten and molybdenum mining activities aggravated the accumulation of heavy metals in nearby farmland and rice (Lin *et al.* 2014).</p><p>Heavy metal pollution of soils in surrounding molybdenum mine area posed a serious threat to ecological environment and residents’ health. Created an unacceptable risk for cancer (Hui *et al.* 2021).</p>||
## **5. Analysis**
China dominates Molybdenum reserves (52%) and production (43%). In all, three countries account for 83% of Molybdenum reserves (China, USA and Peru), as well as 73% of global Molybdenum production (China, Chile and USA).

According to the USGS, identified world resources comprise around 25.4 million tons and are “adequate to supply world needs for the foreseeable future.” (USGS 2022, p. 113). A contrary position is taken by Henckels, Driessen and Worrel (2018, p. 67), who argue that extractable molybdenum resources could be exhausted within fifty to hundred years and thus caution that molybdenum extraction should be reduced by way of increased recycling.

