---
title: T. Silver
description: Critical Minerals in detail  T. Silver
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "T. Silver", "silver"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.038.png)
Analysis by UNEP. All rights reserved.
## **2. Geographical Data**
![Chart, application, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.039.jpeg)

Data source: USGS 2022, p. 153. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Silver<br>(<sup>47</sup> Ag)</b>|Mined by open-pit or underground methods (Great Mining, n.d.). Silver can be mined as a principal product or be obtained as a by-product from lead-zinc, copper, and gold mines (USGS 2022, p. 153).|A silver sulphide concentrate is formed by froth flotation and is then concentrated in a sodium cyanide solution and refined by electrowinning (De Jong, Sauerwein and Wouters 2021).|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Small scale miners emit large amounts of mercury De Lacerda and Salomons 1998).</p><p>**Silver mining is associated with environmental contamination through the emissions of mercury and lead (Robins *et al.* 2012, pp. 627-631).** </p><p>**A significant amount of silver is lost during processing in the form of emissions (specifically Monovalent ionic silver, which possesses antimicrobial properties), ultimately contaminating local water, soil, and food supplies (Eckelman *et al.* 2007, pp. 6283-6289).**</p><p>Historically, silver mining had caused devastating damage to surrounding environments (through water contamination, lack of proper waste disposal, deforestation, and more), and these impacts are still felt in the environment of old mining towns today (Baldwin et al. 2013, p. 1).</p>|<p>Encroachment onto indigenous lands, destroying sacred sites (Boni et al. 2014, pp. 759-780).</p><p>With small scale mines, emissions of toxic materials like lead, mercury, and other silver by-products (the ingestion of Monovalent ionic silver itself has resulted in the development of a mainly cosmetic disorder called argyria) have resulted in the contamination of local food and water supplies, which results in local residents ingesting these toxins, impacting their health (Eckelman *et al.* 2007, pp. 6283-6289).</p><p>Dispossession of rural lands have led indigenous groups and small farmers to protest against large-scale mining projects headed by wealthy corporations who don’t deal with the everyday consequences (ex. Environmental damage, contamination, destruction of culturally significant sites, etc.) of mining (Tetreault 2015, p. 52). </p><p>The issues stemming from the extraction of copper, lead-zinc, and gold resources are relevant when analyzing silver’s impact on communities (De Jong, Sauerwein and Wouters 2021, p. 86).</p>|Silver mining in Mexico had been lucrative due to the country’s lack of enforcement of environmental laws, low taxes, simple administration processes, and lack of royalties (Tetreault 2015, p. 52).|
## **5. Analysis**