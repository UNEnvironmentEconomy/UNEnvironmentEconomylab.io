---
title: A. Aluminium
description: Critical Minerals in detail  A. Aluminium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "A. Aluminium", "aluminium"]

---

## **1. Treatment by Critical Minerals Lists**

![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.001.png)

Analysis by UNEP. All rights reserved.
## **2. Geographical Data**
![Graphical user interface, chart, application, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.002.jpeg)

Data source: USGS 2022, pp. 18, 23, 33. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Mining**|**Refining**|
| - | :- | :- |
|**Aluminium   <br>(13 Al)**|Primarily sourced from the rock bauxite (USGS 2022, p. 33).|Produced from bauxite at aluminium smelters. (USGS 2022, p. 23)|

### **Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>**The processing demands high energy usage and, as by-products, there is a significant release level of Carbon Dioxide and perfluorocarbons, both considered greenhouse effect gases (Andrade-Vieira *et al.* 2019, p. 4).**</p><p>**Spent Pot Liner (SPL) is a solid waste produced during the reduction of bauxite from metallic aluminum (Andrade-Vieira *et al.* 2019, p. 1).**</p><p>**The major problems of SPL are related to the safety of disposal due to the highly leachable nature of its components and the possibility of contaminating freshwater bodies (Andrade-Vieira *et al.* 2019, p. 1).**</p><p>Bauxite reserves are typically found near the earth’s surface, bauxite is usually strip-mined, a method that leads to vast open pits of devastated land, signiﬁcant environmental degradation, and the disruption or even destruction of local wildlife, water ﬂows, and other environmental and eco-logical processes (Gendron, Ingolstad and Storli 2013, p. 13).</p><p>Aluminum is the second most abundant metallic element in the earth’s crust after silicon, accounting for 8% of the earth’s crust (Shaoli, Li and Wang 2016, p. 1).</p><p>**About 60 chemical elements can be found in various complex electronic devices, aluminum being one of them (Heacock *et al.* 2016, p. 551.)**</p><p>Environmental impact of the mining and reﬁning of bauxite is readily apparent in the scars left on the landscape by strip-mining, deforestation, or the holding ponds for red mud (Gendron, Ingolstad and Storli 2013, p. 14).</p><p>**Red bauxite particles are produced from alumina refining of bauxite ore with high pH value and any leaching of these red bauxite particles into water sources resulting in reduced soil fertility and affecting agricultural food products and aquatic life (Rosli *et al*. 2021, p. 2).**</p>|<p>Aluminum is neurotoxic and experimental studies in rats have shown aluminum can accumulate in the cerebral cortex, hippocampus, and cerebellum (Zhang 2021, p. 5).</p><p>Excess aluminum consumption can double the risk of dementia (Zhang 2021, p. 5).</p><p>The destruction of Indigenous and Tribal peoples’ territories from mining development is a form of violence with serious psychosocial impacts (Normann 2022, p. 176).</p><p>The nervous system is considered the main target organ for aluminum toxicity. Aluminum has been shown to be closely related to cognitive dysfunction and Alzheimer's disease (Zhang *et al.* 2022, p. 270).</p>|<p>Bauxite resources in China are inadequate and their grade is also low compared with that from elsewhere in the world. to satisfy domestic demand, China must import substantial raw materials (bauxite and scrap) to produce aluminum (Shaoli, Li and Wang 2016, p. 14).</p><p>At the end of 2019, China's primary aluminum output reached 30.44 million tons, which the number of direct employment in China's aluminum industry ranked first in the world (Zhang *et al.* 2022, p.269).</p><p>According to the OECD, 46% of primary aluminium capacity in the world was under direct government influence in the early 1980s, either through state ownership or equity participation (Nappi 2013, p. 12).</p><p>The primary aluminium industry of today has little resemblance to what it was 40 years ago. Brazil, Russia, India, and China’s economies now account for more than 40% of bauxite production, while alumina output has shifted towards bauxite-rich countries and away from industrialized economies (Nappi 2013, p. 25).</p><p>Land reclamation projects demanded by the host countries have achieved limited results, and so the arrival of prospectors can also bring about unusual coalitions between farmers and environmentalists in opposition to the corporations (Gendron, Ingolstad and Storli 2013, p. 13).</p>|

## **4. Analysis**
The USGS estimates the global resources of bauxite at 55–75 billion tons as “sufficient to meet world demand for metal well into the future” (USGS 2022, p.23).

Aluminium appears on 6 of the 8 critical mineral lists studied. Given the fact that bauxite reserves are deemed to be sufficient to cater for medium to long term future demand, that there is a fair geographic distribution of such bauxite reserves, and that bauxite resources by region is diversified, there does not seem to be undue reason for concern insofar access to bauxite or the production of sufficient quantities of aluminium from bauxite is concerned. However, it is suggested that aluminium be placed on a watchlist, for the following reasons:

- 72% of mining production of aluminium is concentrated in the hands of just three countries: Australia (28%), China (22%) and Guinea (22%).
- 57% of smelter production is concentrated in the hands of a single country: China.


