---
title: H. Germanium
description: Critical Minerals in detail  H. Germanium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "H. Germanium", "germanium"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.014.png)

Analysis by UNEP. All rights reserved.

## **2. Geographical Data**
![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.015.jpeg)

Data source: USGS 2022, p. 71. Analysis by UNEP. All rights reserved.

## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Germanium<br>(<sup>32</sup> Ge)</b>|Germanium is mainly produced as a by-product of the mining of zinc (USGS 2018, p. 4; White et al. 2022, p. 1). In addition, germanium is acquired from germanium-bearing coal ash and recycling (Melcher and Buchholz 2014, p. 196).|<p>Recovering germanium from lignite requires burning the coal at 1200°C and subsequent leaching with sulfuric acid (Melcher and Buchholz 2014, p. 188). Studies have been conducted to propose more environmentally friendly methods to recover germanium from coal fly ash (Rezaei *et al.* 2022).</p><p>Recovering germanium from zinc oxide dust typically used sulfuric acid as a leaching agent. However, more recent studies have suggested alternative methodologies to recover germanium (Xi *et al.* 2021).</p>|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Arsenic and cadmium produced as a result of processing materials containing germanium pose the greatest environmental risks (Melcher and Buchholz 2014, p. 192).</p><p>**Around a germanium mine in Xilinhot, China, arsenic, cadmium, and lead pollution was distributed throughout the area around the mine (Zhenhua *et al.* 2019, p. 761).** </p><p></p>|<p>The consumption of germanium dietary supplements has been linked to adverse health outcomes including kidney dysfunction, muscle weakness and death (Tao and Bolger 1997).</p><p>Coal fly ash deposits which contain germanium pose threats to human health as they may be radioactive, toxic, are soluble and may contaminate nearby waters and soils (Rezaei *et al.* 2022, p. 1).</p>||

## **5. Analysis**
It has been predicted that future shortages of germanium are likely (Zhang and Xu 2018, p. 1002). The recovery of germanium from coal ash at coal-fired plants could increase the availability of germanium  on the medium term if additional coal mines are produced (Melcher and Buchholz 2014, p. 199), such as seems likely in countries such as China, South Africa and India (see the section on power generation in Chapter 1). However, this is hardly an outcome that this publication is advocating for.

