---
title: E. Cobalt
description: E. Cobalt
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "E. Cobalt", "cobalt"]

---

## **1. Treatment by Critical Minerals Lists**

![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.008.png)
Analysis by UNEP. All rights reserved.

## **2. Geographical Data**
![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.009.jpeg)

Data source: USGS 2022, pp. 18, 53. Analysis by UNEP. All rights reserved.

## **3. Production and Processes**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Cobalt <br>(<sup>27</sup> Co)</b>|Except for cobalt production in Morocco and artisanally mined cobalt in Congo (Kinshasa), most cobalt is mined as a by-product of copper or nickel (USGS 2022, p. 53).|Separated from copper-nickel concentrate by froth flotation (Young 2020).|

## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Cobalt becomes toxic in high quantities. The release and concentration of cobalt into the air, soil, and streams surrounding mine and refinery sites are cause for concern.</p><p>**Emissions of Sulfur dioxide (which causes acid deposition) and metalliferous particles produced by the Cu-smelting industry for over 80 years in the vicinity of the city of Lubumbashi have degraded woodland and caused its replacement by open grassland and bare soil in the area situated downwind of the copper smelter (Pourret *et al.* 2016, p. 44) .**</p><p>In response to greenhouse gas abatement efforts, cobalt demand is expected to more than triple over the coming decade, with a shift towards electric vehicles (Becker 2021, p. 1).</p><p>**The vitamin B12, also called cobalamin (due to its cobalt nucleus), is involved in the metabolism of all animals and many phytoplanktonic species, and stimulates in hospite zooxanthellae growth (Biscéré *et al.* 2015, p. 2).**</p><p>**When their temperature threshold has been exceeded or when corals are submitted to a poor seawater quality, it has been widely demonstrated that they stop their growth and expel their zooxanthellae and so a cobalt pollution would exacerbate these phenomena (Biscéré *et al.* 2015, p. 14).**</p><p>**A potentially significant future source of cobalt is secondary resources such as mine tailings or smelter slags. Recycling cobalt, especially from spent Li-ion batteries is one of the major challenges for the future of cobalt supply for countries with very little domestic cobalt reserves (Dehaine *et al.* 2021, p. 20).**</p><p>**Deposits where cobalt is the dominant metal often contain a high content of arsenic (e.g., Bou Azzer in Morocco, Blackbird in the United States) or other penalty elements like uranium (e.g., Sotkamo in Finland) (Dehaine *et al.* 2021, p. 22).**</p>|<p>Concentrations of Co and other metals were higher in the urine of people living close to mines or smelting plants (including the site contaminated by the Cu-smelter industry of Lubumbashi), exceeding the baseline value of the Centers for Disease Control and Prevention (Pourret *et al.* 2016, p. 44).</p><p>Cobalt exposure occurs in cobalt mining. Exposure to cobalt can cause negative effects to the pulmonary, hematological, endocrine, and nervous system. All those exposures have multiple adverse health outcomes, including serious social implications (Landrigan *et al.* 2022, p. 2).</p><p>Indication exists that birth defects of children are related to cobalt mining (Landrigan *et al.* 2022, p. 7).</p><p>Child labour, forced labour, corruption and human rights abuse (Michaels, Maréchal and Katz 2022).</p>|Corruption, non-state armed groups, abuse of force of security forces and non-payment of taxes (Michaels, Maréchal and Katz 2022).|

## **5. Analysis**
Nowadays, the global cobalt market is dominated by the rechargeable battery industry which is responsible for 58% of cobalt use (Dehaine *et al.* 2021, p. 2).

The production of refined cobalt metal is currently dominated by China (~67%), followed by Finland (~11%) and Canada (~5%). This implies that more than two-thirds of refined cobalt is produced in countries where no cobalt mining takes place (Dehaine *et al.* 2021, p. 2).

A history of supply shortages and sharp increases in cobalt price highlight two major risks to the global and U.S. economies. First, achievement of greenhouse gas abatement and other environmental goals over the long-term could be thwarted should cobalt become more expensive, as mining goes from higher quality to lower quality resources. Second, there is a risk that a supply-side or demand-side disruption could lead to a short-term change in global cobalt availability — which could arise unintentionally and affect all users of cobalt similarly (Becker 2021, p. 1).

The impacts of this increased demand are expected to be massive in countries such as the Democratic Republic of Congo, which holds roughly half of the world’s cobalt reserves (Landrigan *et al.* 2022, p. 3).

In order to meet future demand, the mining industry will have to address a series of challenges which can be grouped into 3 main themes: (a) cobalt resources, (b) cobalt resource efficiency and processing technology, and (c) the socio-environmental aspects of cobalt extraction (Dehaine *et al.* 2021, p. 20).

The USGS puts identified world terrestrial resources at around 25 million tons, with the vast majority being in the Congo (Kinshasa), Zambia, Australia, Canada, Cuba, Russia and the United States (USGS 2022, p. 53). In excess of 120 million tons of cobalt resources have been identified in polymetallic nodules and crusts on the floor of the Atlantic, Indian, and Pacific Oceans (USGS 2022, p. 53); however, see the concerns raised regarding deep sea mining in Chapter 1.

