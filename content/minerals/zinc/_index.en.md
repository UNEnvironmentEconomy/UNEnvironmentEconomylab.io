---
title: Y. Zinc
description: Critical Minerals in detail  Y. Zinc
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "Y. Zinc", "zinc"]

---

## **1. Treatment by Critical Minerals Lists**

![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.047.png)

Analysis by UNEP. All rights reserved.

## **2. Geographical Data**
![Graphical user interface, chart, application, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.048.jpeg)

Data source: USGS 2022, p. 193. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Zinc<br>(<sup>30</sup> Zn)</b>|<p>Mined using underground and open-pit methods (Great Mining n.d.).</p><p>Sphalerite, which tends to occur with galena (ore for lead and potentially silver). This results in ‘lead-zinc’ or ‘lead-zinc-silver' mines, meaning that similar issues may arise between the two minerals (De Jong, Sauerwein and Wouters 2021, p. 86).</p>|Zinc is beneficiated by froth flotation after addition of copper sulphate. Concentrate is smelted before acid leaching and electrowinning (De Jong, Sauerwein and Wouters 2021, p. 86).|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Major environmental issues are soil and water contamination (De Jong, Sauerwein and Wouters 2021, p. 86).</p><p>**Once zinc finds its way into bodies of water, it can pose a risk to aquatic organisms, ultimately impacting ecosystems (Environment and Climate Change Canada Health Canada 2019).**</p><p>**Crop pollution results from soil/water pollution, resulting in crops with a significantly higher heavy metal content (Zhang *et al.* 2012).** </p>|<p>For workers, prolonged exposure to zinc can cause metal fever (Greenspec n.d.).</p><p>Excessive consumption of zinc (through water, food, or air near mines) can result in anemia, nervous system disorders, and damage to the pancreas, and many more negative side effects. (Nriagu  2007).</p><p>Increased zinc ingestion may also be related to infertility (Zhang *et al.* 2012, p. 2262).</p>|Regulation in China may be lax, allowing for additional environmental concerns (Zhang *et al.* 2012, p. 2270). |
## **5. Analysis**
The USGS puts identified world zinc resources at around 1.9 billion tons (USGS 2022, p. 193).


