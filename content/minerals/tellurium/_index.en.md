---
title: U. Tellurium
description: Critical Minerals in detail  U. Tellurium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "U. Tellurium", "tellurium"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.040.png)

Analysis by UNEP. All rights reserved.

## **2. Geographical Data**

No geographical data is available from the USGS on Tellerium.

## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Tellurium<br>(<sup>52</sup> Te)</b>|Tellurium is frequently released into the environment as a by-product of metal production, as well as from the mining and burning of coal and oil (Wiklund et al 2018).||

## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Current knowledge about tellurium behaviour in the environment is strongly hindered by analytical difficulties, with insufficiently low analytical detection limits being the main limitation. </p><p>Values for tellurium concentrations in the different environmental compartments are scarce. </p><p>Reliable estimates of tellurium concentrations in seawater and freshwater cannot be produced (Filella et al. 2019, p. 215).</p><p>**Some urban soil surveys and lake sediment data close to tellurium contamination sources point to possible effects on the element’s distribution as a result of human activity; long-range atmospheric transport remains to be proved (Filella *et al.* 2019, p. 215).**</p>|Lack of data on availability and impacts of tellurium mean the social impacts are largely unknown. The future impacts cannot be estimated with the limited amount of data available. ||
## **5. Analysis**
There is limited knowledge on the availability and impacts of tellurium. 

Current knowledge about tellurium behaviour in the environment is strongly hindered by analytical difficulties, with insufficiently low analytical detection limits being the main limitation. 

The limited information available on tellurium make governance of it difficult to establish. 

Overcoming the current analytical limitations is essential to be able to progress in the field. (Filella *et al.* 2019, p. 215).
