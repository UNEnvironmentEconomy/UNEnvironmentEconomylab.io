---
title: P. Nickel
description: Critical Minerals in detail  P. Nickel
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "P. Nickel", "nickel"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.030.png)
Analysis by UNEP. All rights reserved.
## **2. Geographical Data**

![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.031.jpeg)

Data source: USGS 2022, p. 115. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Nickel<br>(<sup>28</sup> Ni)</b>|Found as sulphides and laterites. Sulphides extracted by underground mining and laterites by open pit mining (Great Mining n.d.).|Sulphides are separated by flotation followed by smelting and electrolysis.  Laterites are dried and smelted in an electric furnace (Great Mining n.d.).|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Many of the world’s Nickel resources are located across the Pacific archipelago – an area that has seen rapid growth in Nickel mining in the past decade. The increased mining has further exacerbated pressures on the wide variety of important biodiversity and vulnerable ecosystems (Mudd and Jowitt 2022).</p><p>Surface water or groundwater resources can be impacted in a number of ways by Nickel projects, such as extraction, water discharges, pollution, strong rates of erosion, acid mine drainage, and contaminated mine-site waters (Mudd and Jowitt 2022).</p>|<p>Child labour, forced labour,  corruption and human rights abuse (Michaels, Maréchal and Katz 2022).</p><p>Nickel contact can cause a variety of side effects on human health, such as allergy, cardiovascular and kidney diseases, lung fibrosis, lung and nasal cancer (Genchi *et al.* 2020).</p><p>Depending on the dose and length of exposure, as an immunotoxin and carcinogen agent, Ni can cause a variety of health effects, such as contact dermatitis, cardiovascular disease, asthma, lung fibrosis, and respiratory tract cancer (Costa *et al.* 1994, pp. 127-130).</p>|<p>Much of the world’s nickel is in developing countries with active indigenous communities who remain skeptical or openly hostile on environmental risks and impacts from nickel-producing projects (Mudd and Jowitt 2022).</p><p>Globally, there has been the withdrawing of the social license to operate within the nickel sector due to worsening relationships between companies governments and communities (Mudd and Jowitt 2022).</p><p>Corruption and non-payment of taxes (Michaels, Maréchal and Katz 2022).</p>|
## **5. Analysis**
According to the USGS, identified land-based resources amount to at least 300 million tons of nickel (USGS 2022, p. 115). There also are extensive nickel resources to be found in manganese crusts and nodules on the ocean floor (USGS 2022, p. 115). – see in this context the caution against deep-sea mining contained in Chapter 1.

