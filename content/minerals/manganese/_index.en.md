---
title: N. Manganese
description: Critical Minerals in detail  N. Manganese
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "N. Manganese", "manganese"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.026.png)

Analysis by UNEP. All rights reserved.
## **2. Geographical Data**

![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.027.jpeg)

Data source: USGS 2022, pp. 18, 107. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Manganese<br>(<sup>25</sup> Mn)</b>|Over 460 known minerals contain manganese. Sourced from open pit mines and potentially from deep sea mining.|Smelted to produce ferromanganese, which is reduced with carbon to produce the metal. (Great Mining n.d.).|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Destruction of seabed and marine habitat (Sharma 2011, p. 28).</p><p>Manganese coatings on natural surfaces (such as rocks) can further act as “traps” for metals in mine waters and streams (Neculita and Rosa 2019, p. 496).</p><p>Particulate Manganese matter derive from combustion sources, such as chimney of the processing plan (Charron 2012, p. 92).</p><p>Mean outdoor airborne Mn levels double the US-EPA recommended level within the mining region (Charron 2012, p. 89).</p>|<p>Effect of deep sea mining on local islanders' culture and livelihood (Fainu 2021).</p><p>High Mn concentrations significantly associated with neurobehavioral and cognitive  conditions (Charron 2012, pp. 90-91).</p><p>Manganese concentrations 20x greater in children within the mine's region, as opposed to the control population (Charron 2012, p. 91).</p><p>Resuspended dust from vehicular traffic and unpaved road erosion significant source of atmospheric manganese (Charron 2012, p. 92).</p><p>Inhalation of manganese particulate matter (Charron 2012, pp. 89-90).</p><p>Inhalation of airborne manganese can result in permanent neurological disorders (Neculita and Rosa 2019, p. 492).</p><p>Links between intellectual impairment and manganese levels in drinking water (Neculita and Rosa 2019, p. 492).</p>|<p>Lack of transparency between state government and affected communities on enforcing government regulations can create distrust and undermine the government’s protective efforts (Charron 2012, pp. 93-94).</p><p></p>|
## **5. Analysis**
72% of global production derives from just three countries (South Africa, Gabon and Australia). Similarly, 79% of reserves are held by three countries (South Africa, Australia and Brazil). This geographical concentration could lead to bottlenecks down the supply chain, for instance in the event of political instability in one of those countries.

The greatest abundance is in the form of ferromanganese nodules and crusts on the ocean floor (USGS 2022, p. 107) – see Chapter 1 on risks associated with deep sea mining.

Relatively high supply risk score due to lack of substitutability (Murdock, Toghill and Tapia-Ruiz 2021, p. 9).

