---
title: J. Graphite
description: Critical Minerals in detail  J. Graphite
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "J. Graphite", "graphite"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.018.png)
Analysis by UNEP. All rights reserved.
## **2. Geographical Data**
![Graphical user interface, chart, application, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.019.jpeg)

Data source: USGS 2022, p. 75. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Graphite<br>(<sup>6</sup> C)</b>|Commonly occurs as masses or flakes and may be a component of many different metamorphic rocks. |Separated by flotation and gravity separation (Michael 2016). Processing may be done without use of chemicals.|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Acid-rock drainage from tailings.</p><p>Graphite powder become airborne and lead to air pollution and deposition of graphite powder (Whoriskey 2016).</p><p>Graphite production discharges pollutants, such as hydrofluoric acid, into local water sources (Whoriskey 2016).</p><p>Crop yields decreased due to deposited airborne graphite pollution (Whoriskey 2016).</p>|Inhalation of graphite powder during mining can lead to the lung condition, graphite pneumoconiosis (Ranasinha and Uragoda 1972, pp. 178-183).||
## **5. Analysis**
Current mined production is concentrated in China (>65%) (Olivetti *et al.* 2017, p. 233). Synthetic graphite is more expensive to produce (some estimate double) (Olivetti *et al.* 2017, p. 233). According to the USGS, global identified graphite resources exceed 800 million tons of recoverable graphite. (2022, p. 75)<sup>.</sup>

