---
title: M. Lithium
description: Critical Minerals in detail  M. Lithium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "M. Lithium", "lithium"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.024.png)
Analysis by UNEP. All rights reserved.
## **2. Geographical Data**

![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.025.jpeg)

Data source: USGS 2022, pp. 18, 101. Analysis by UNEP. All rights reserved.
## **3.Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Lithium<br>(<sup>3</sup> Li)</b>|Produced from brine extraction and hard rock mining (Great Mining n.d.).|Precipitated from brine evaporation or solvent extraction (Dry 2018).|

## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Large Consumption of water/water loss (U.S. Bureau of Land Management [USBLM] 2020, p. 57); Murdock, Toghill and Tapia-Ruiz 2021, p. 10).</p><p>Antimony and arsenic contamination of ground water (USBLM 2020, pp. 41, 63).</p><p>Mining waste may contain modest radioactive uranium (USBLM 2020, pp. 159, 165).</p><p>Mining waste loaded with sulfuric acid (USBLM 2020, p. 159).</p><p>**Degradation of species habitat and land (USBLM 2020, p. 88; Araujo, Bento and Silva 2022, p. 7).** </p><p>Flamingo species at risk due to lithium mining in Andes (Buehler 2022).</p><p>Water loss from evaporation (Dry 2018).</p>|<p>Studies on the impacts to local communities near lithium mining are relatively scarce in the developing lithium triangle region. (Chaves *et al.* 2021, p. 3).</p><p>Agricultural and pastoral activities are affected by environmental degradation (Bolivia) (Chaves *et al.* 2021, pp. 3-4).</p><p>Child labour, forced labour,  corruption and human rights abuse (Michaels, Maréchal and Katz 2022). </p><p>Opposition from nearby communities – conflicts over water rights and resources (Chile) (Seefeldt 2022).</p><p>Opposition to lithium mining from local communities (Dorn 2021, pp. 70-91).</p><p></p>|Corruption and non-payment of taxes (Michaels, Maréchal and Katz 2022).|
## **5. Analysis**
According to the USGS, identified world lithium resources comprise around 89 million tons (USGS 2022, p. 101). 

Supply risk due to lack of sufficient recycling opportunities (Olivetti *et al.* 2017, p. 233; Murdock, Toghill and Tapia-Ruiz 2021, p. 8).

Challenge of Lithium production is not whether there is enough material but whether production can ramp up quickly Olivetti *et al.* 2017, p. 234).

50% of global consumption  in China Olivetti *et al.* 2017, p. 234).


