---
title: X. Vanadium
description: Critical Minerals in detail  X. Vanadium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "X. Vanadium", "vanadium"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.045.png)

Analysis by UNEP. All rights reserved.

## **2. Geographical Data**

![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.046.jpeg)

Data source: USGS 2022, pp. 18, 183. Analysis by UNEP. All rights reserved.

## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Vanadium<br>(<sup>23</sup> V)</b>|<p>Typically recovered as a by-product or coproduct (Hammarstrom et al. 2022, p. 2).</p><p>Titaniferous magnetite can be found in other ore bodies as well (De Jong, Sauerwein and Wouters 2021, p. 84).</p>|<p>When in association with magnetite ore, the ore is reduced and smelted.</p><p>When present as an oxide with HREEs, ore is heap-leached then further processed by solvent extraction (Great Mining n.d.).</p>|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Mining and processing can lead to higher quantities being present in water, earth and atmosphere. Potential for AMD generation.</p><p>**Vanadium mining results in topsoil pollution near the extraction site (Yanguo *et al.* 2006).**</p><p>**Significant vanadium exposure results in destabilization of plant physiological balance, leading to a reduction in potential yield and general damage to plant growth/development (Hanus-Fajerska *et al.* 2021).** </p><p></p>|Human exposure to vanadium mostly occurs through the consumption of contaminated food (the public) or the inhalation (occupational) of V<sub>2</sub>O<sub>5</sub> dust during the production of FeV and steel. Such inhalation may cause extended coughing and is suspected to cause cancer. Vanadium toxicity is believed to result from an intake of more than 10-20 mg of V per day.|<p>Severe knowledge gaps on the impacts of vanadium mining on the environment and beings (lack of knowledge regarding how the mineral interacts when faced with physical/chemical changes). These gaps led to the lack of regulation surrounding acute exposure to vanadium (Watt *et al.* 2018).</p><p></p>|
## **5. Analysis**
There is very high geographical concentration in respect of Vanadium: just 4 countries hold all global reserves (China, Australia, Russia and South Africa), while China leads global production at 66%, with Russia second at 11%.

` `According to the USGS, there are in excess of 63 million tons world vanadium resources. However, due to the fact that vanadium is typically recovered as a byproduct or a coproduct, “demonstrated world resources of the element are not fully indicative of available supplies.” (USGS 2022, p, 183).

