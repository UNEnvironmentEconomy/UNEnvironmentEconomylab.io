---
title: V. Tin
description: Critical Minerals in detail  V. Tin
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "V. Tin", "tin"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.041.png)

Analysis by UNEP. All rights reserved.
## **2. Geographical Data**

![Graphical user interface, chart, application, Excel, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.042.jpeg)

Data source: USGS 2022, p. 175. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Tin<br>(<sup>50</sup> Sn)</b>|||
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Onshore mining:</p><p>**Onshore tin mining can cause loss of soil fertility in farmland area, pollution of drinking water, and progressive incursion of mining activities into protected forests and conservation areas (Diprose *et al.* 2022, p. 799).**</p><p>During exploration/mining trenching and pitting adversely affects the landscape and hydrosphere (Omotehinse and Ako 2019, p. 21).</p><p>Mining can cause the formation of wasteland, damage to natural drainage, and the pollution and destruction of the natural habitat (Omotehinse and Ako 2019, p. 21).</p><p>Mining can result in mining pools, thereby enhancing erosion. Additionally, such pools contain large amounts of tailings and are therefore unable to support any form of living things (Omotehinse and Ako 2019, p. 21).</p><p>Enhanced erosion can lead to soil degradation (Omotehinse and Ako 2019, p. 21).</p><p>Offshore mining:</p><p>**Silt and sludge from offshore mining boats can produce serious environmental impacts for the ocean environment and fisheries, killing species and habitats (Diprose *et al.* 2022, p. 799).**</p>|<p>Dependency on mining for employment can lead to extreme socio-economic downfall following mine closure (Syahrir, Wall and Diallo 2020, p. 1529).</p><p>Tin mining has been associated with occupational health and safety concerns, particularly child labour and unsafe mining practices amongst artisanal miners (Diprose *et al.* 2022, p. 799).</p><p>Environmental degradation threatens the livelihoods of locals dependent on resource-based industries, like fishing and tourism (Diprose *et al.* 2022, p. 799).</p><p>An influx of workers seeking jobs in tin mining can lead to conflict between local communities and artisanal workers (Ibrahim and Wahyudin 2018, p. 2).</p><p></p>|<p>Artisanal mining makes governance difficult to establish and enforce. A low level of enforcement can lead to low environmental awareness among people, operators, and regulators (Nurtjahya and Agustina 2015).</p><p>Transnational efforts to promote the localization of sustainability norms have made small contributions, however they are limited in scope and difficult to sustain within highly dynamic local political environments (Diprose *et al.* 2022, p. 802).</p><p>Power dynamics between local and national governments can lead to uneven governance capacity (Diprose *et al.* 2022, p. 805).</p>|
## **5. Analysis**
Tin resources and production of tin are highly geographically concentrated: China and Indonesia jointly account for 54% of global production, while three countries hold 52% of global resources: China, Indonesia and Myanmar.
