---
title: R. Rare Earth Elements [REEs]
description: Critical Minerals in detail  R. Rare Earth Elements [REEs]
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "R. Rare Earth Elements [REEs]", "rare_earth_elements"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.034.png)
Analysis by UNEP. All rights reserved.
## **2. Geographical Data**

![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.035.jpeg)

Data source: USGS 2022, pp. 18, 135. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|**Rare Earth Elements (REEs)**|<p>Heavy mineral sands, as well as hard rock REE occurrences.</p><p>Light Rare Earth Elements (LREEs) are La-Gd (<sup>57-64</sup>) on the periodic table; Tb-Lu (<sup>65-71</sup>) are Heavy Rare Earth Elements (HREEs). Some also include the transition metals Scandium (<sup>21</sup> Sc) and Yttrium (<sup>39</sup> Y) as REEs.</p>|REEs are processed using magnetic and/or electrostatic separation followed by flotation. The concentrate is acid leached before solvent extraction (Geschneider and Pecharsky n.d.).|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|**Rare earth mining can be toxic and radioactive. Under-regulated rare earths projects can produce wastewater and tailings ponds that leak acids, heavy metals and radioactive elements into groundwater (Ives 2013).**|<p>Workers exposed to high concentrations face lung and heart complications (Mother, Palmer and Setton n.d.).</p><p>Ingestion, inhalation, and dermal exposure for workers and surrounding residents (including potentially Indigenous communities) and subarctic and arctic climate conditions could increase the risks to human and ecological health in future REE production development in Canada (Xiangbo *et al.* 2020).</p>||
## **5. Analysis**
REEs are extremely crucial to the green energy transition. They are one of only two elements to be found on every one of the 8 critical minerals lists studied – the other was nickel.

Although REEs are relatively common in the earth’s crust, they are less frequently found in minable concentrations than most other minerals (USGS 2022, p. 135).

China has dominance over the REE industry. It appears as though China is now moving its operations to Africa, where it can contaminate outside communities instead of exposing its citizens at home to the risks of REE mining. Though African nations accept these deals now, some worry that this is a long-term strategy for China to lock African nations into a cycle of debt (Nayar 2021).

China’s dominance is visible in both the production and reserves sphere: it is the country with the largest single share of REEs at 37% and its global production is at the order of 60% of all REEs.

