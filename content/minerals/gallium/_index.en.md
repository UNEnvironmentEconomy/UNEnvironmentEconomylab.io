---
title: G. Gallium
description: Critical Minerals in detail  G. Gallium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "G. Gallium", "gallium"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.012.png)

Analysis by UNEP. All rights reserved.

## **2. Geographical Data**
![Diagram Description automatically generated with low confidence](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.013.jpeg)

Data source: USGS 2022 at 65. Analysis by UNEP. All rights reserved.

## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Gallium<br>(<sup>31</sup> Ga)</b>|Gallium production is largely dependent on the refinement of aluminum and zinc from bauxite and sphalerite deposits, respectively (Eheliyagoda *et al.* 2019, p. 333). 90% of gallium is produced as a by-product of aluminum production, making gallium production reliant on the market for aluminum (Song et al. 2022, p. 2700). However, the primary aluminum production, China (which is also the main producer of gallium), is expected to peak and drop, leading to a decrease in primary aluminum production (Song et al. 2022, pp. 2700-2701).||

## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>Concentrations of gallium have been shown to be 1000x higher in the groundwater near a semiconductor industrial park than in another nearby area. Similarly, gallium was measured to be 2.5x higher downwind of a semiconductor industrial complex as opposed to upwind the complex (White and Shine 2016, p. 463). Gallium levels have also been found to be elevated in the acid mine drainage from zinc mining than in local California streams (White and Shine 2016, p. 463).</p><p></p>|<p>The toxicity of gallium on humans has not been studied significantly (White and Shine 2016, p. 463). However, elevated levels of gallium have been observed in the urine and blood of semi-conductor workers (White and Shine 2016, p. 463). Two independent case studies have suggested that acute exposure to gallium produced toxic effects (White and Shine 2016, p. 463). As well, researchers suggest that gallium’s use as a chemotherapeutic agent gives insight into its potential toxic effects to human beings (White and Shine 2016, p. 464). Animal studies have also shown that exposure to gallium may be toxic (White and Shine 2016, p. 464).</p><p>Exposure to gallium is common for operators and engineers in semi-conductor factories. In one particular factor, these workers were exposed to 30 000X the amount of gallium as what human is normally (White and Shine 2016, p. 463). Other areas of high local concentrations are mines and smelters, recycling facilities and coal fired power plants (White and Shine 2016, p. 463).</p>||

## **5. Analysis**

Gallium production plays an important role in the Chinese economy, as China supplies over 70% of global demand. It has been recommended that priority be given to developing gallium recycling technologies as reserves of gallium decrease (Eheliyagoda *et al.* 2019, p. 332). By the year 2050, it is estimated that gallium production would need to be increased by 224% compared to 2010 levels (Eheliyagoda *et al.* 2019, p. 333). End of life recycling of gallium is less than 1% globally and it has been recommended that countries develop policy to regulate the gallium industry and the recycling of gallium (Eheliyagoda *et al.* 2019, pp. 336-337). Projections have suggested that the long-term sustainability of gallium resources is not optimistic. (Eheliyagoda *et al.* 2019, p. 339).

Gallium is at a high risk for supply chain vulnerability due to limited number of geographical regions and gallium productions reliance on by-product extraction systems (Althaf and Babbitt 2021, pp. 11-12). In addition, finding suitable substitutes for gallium is difficult. (Song et al. 2022, p. 2700).

