---
title: D. Chromium
description: Critical Minerals in detail  Critical Minerals in detail  D. Chromium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "D. Chromium", "chromium"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.006.png)

Analysis by UNEP. All rights reserved.
## **2. Geographical Data**
![Chart, application, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.007.jpeg)

Data source: USGS 2022, pp. 18, 49. Analysis by UNEP. All rights reserved.

## **3. Production and Processes**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Chromium<br>(<sup>24</sup> Cr)</b>|Mined by both open-pit and underground mining methods (Great Mining n.d.).|Processed by both gravity and magnetic separation or a combination of both (911 Metallurgist 2017).|

## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>The effluents and solid wastes from the mining, chrome-plating, leather-tanning, and dye-manufacturing industries are high in chromium concentration and identified as a major health hazard because of pollution to the environment. The Ferrochrome industry is one of the biggest contributors of the chromium pollution to the water bodies (Coetzee, Bansal and Chirwa 2020, p. 51).</p><p>Chromium in atmosphere is removed by rain, as studies showed approximately all chromium in atmosphere comes back with rain (Coetzee, Bansal and Chirwa 2020, p. 53).</p><p>**Cr(VI) has been shown to have several negative environmental impacts, e.g. reduced germination and growth of some plants, increased mortality and reproduction rates in earthworms, organ damage in crayfish, detrimental effects on survivability, growth and postexposure reproduction of marine fish larvae and copepods, toxic effects on gill, kidney and liver cells of fresh water fish, and possible diatom demise (Beukes *et al.* 2017, p. 875).**</p><p>**Considering that Cr(VI) has been reported to occur/form naturally in some environments, it is vital to properly assess the possible occurrences of natural Cr(VI) in surface and ground-waters, and soils before large-scale chromite mining commences in the Ring of Fire (Beukes *et al.* 2017, p. 878).**</p><p></p>|<p>Chromium IV may cause allergic reactions, skin rashes, kidney and liver problems (Sneddon 2004).</p><p>Chromium has long been recognized as a toxic, mutagenic and carcinogenic metal. It is toxic to microorganism, plants, animals and humans (Coetzee, Bansal and Chirwa 2020, p. 51).</p><p>Chromium is an essential nutrient for plant and animal metabolism; however, when accumulated at high levels, it can cause serious health problems. Chromium is very toxic and is considered as a Class A carcinogen by the U.S. EPA (Coetzee, Bansal and Chirwa 2020, p. 55).</p><p>Ferrochrome production emits air pollutants such as nitrogen oxides, carbon oxides and sulfur oxides and particulate dusts that contain heavy metals such as chromium, zinc, lead, nickel and cadmium. Health risks via inhalation are also a concern (MiningWatch Canada 2012, p. ii).</p><p>Human exposure pathways to chromium are inhalation, ingestion and skin contact (MiningWatch Canada 2012, p. v).</p><p>Observed toxic effects of chromium compounds to humans or laboratory animals include developmental issues, damage to skin, respiratory, reproductive and digestive systems and cancer (MiningWatch Canada 2012, p. vi.)</p><p>Chronic low-level skin exposure to Cr-III or Cr-VI can cause permanent sensitisation that leads to a skin condition called allergic contact dermatitis (MiningWatch Canada 2012, p. ii).</p>|<p>The chromite mining as well as the overall mining sector in South Africa has historically been a crucial factor in the economic growth and advancement of the country. South Africa (SA) holds 72–80% of the world’s viable chromite ore reserves. U.S. Geological survey showed the South Africa was the biggest chromite mine producer in the year 2016 (Coetzee, Bansal and Chirwa 2020, p. 52).</p><p>The leachable Cr(VI) concentrations in both stainless steel and ferrochrome fine dusts are reported to exceed the regulation limits of 0.05 mg/L for Cr(VI) in drinking water and 2 mg/L total limit in South Africa (Coetzee, Bansal and Chirwa 2020, p. 53).</p><p>Chromium trioxide and chromic acid # 33 are recommended for addition to Schedule 1 of the Environmental Emergency Regulations under Part 3 with a threshold quantity of 0.22 tonnes (Environment Canada 2011, p. 3).</p><p>Significant deposits of chromite occur in several countries such as Finland, Kazakhstan and Turkey. Only 31 250 tons of chromite was produced in North America during 2012 (Beukes *et al.* 2017, p. 875).</p>|

## **5. Analysis**
The USGS estimates global shipping-grade chromite sources to be in excess of 12 billion tons, “sufficient to meet conceivable demand for centuries.” (USGS 2022, p. 49). However, their heavy geographic concentration (95% in Kazakhstan and southern Africa (USGS 2022, p. 49)) constitutes a risk factor. Fairly recently very large chromite reserves were discovered in the Ring of Fire in Canada and will likely be exploited due to the strategic importance of stainless-steel. The Ring of Fire is also located within the traditional territories of several First Nations groups and exploitation will require consultation (Beukes *et al.* 2017, p. 875).

