---
title: I. Gold
description: Critical Minerals in detail  I. Gold
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "I. Gold", "gold"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.016.png)
Analysis by UNEP. All rights reserved.
## **2. Geographical Data**
![Graphical user interface, chart, application, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.017.jpeg)

Data source: USGS 2022, p. 73. Analysis by UNEP. All rights reserved.
## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Gold<br>(<sup>79</sup> Au)</b>|||
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>**Mine tailings composed of heavy metals, cyanide, metalloids, radioactive metals and sulphide minerals pose risks to the environment. Cyanide commonly used in the leaching process is highly toxic. Cyanide poisoning of the environment after tailings leakage has been recorded on multiple occasions, contributing the death of local flora and fauna (Anning *et al.* 2019, pp. 1120-1121).**</p><p></p><p>Gold mining in Ghana poses risks to local farmland as environmental degradation persists.[^1]. The contamination of land with mercury is also prevalent, as the mercury is used to process the ore (Sengupta 2021, pp. 261-262).</p><p>Mining is Ecuador is raising concerns about the discharge of cyanide and mercury into local rivers and the bioaccumulation about those metals in fish (Sengupta 2021, pp. 262-263).</p><p>Gold mine dam’s containing toxic waste can burst causing significant damage to the local ecosystem. For instance, in 200 in North-Western Romania, a dam containing toxic waste burst releasing 100 000 cubic metres of waste water which killed 80-90% of the local rivers fish stocks (Sengupta 2021, p. 263).</p>|<p>Cyanide used in the leaching process is highly toxic to humans and individuals exposed to high concentrations of cyanide (through inhalation, oral and dermal contact) are likely to die within a few minutes (Anning *et al.* 2019, p. 1120).</p><p>Mining operations have resulted in the destruction of local farmland and contributed to incidents of food insecurity and malaria. Increased incidents of contaminated drinking water have also been reported (Hausermann, Adomako and Robles 2020). </p><p>The release of cyanide and mercury from toxic waste dams pose serious threats to human health (Sengupta 2021, pp. 261-263).</p><p>Financially vulnerable children in Ghana have been reported as leaving school to work in the mines, exposing themselves to dangerous conditions (Hausermann, Adomako and Robles 2020, p. 66).</p><p></p><p></p>|<p>Environmental and geopolitical concerns have contributed to the opposition of gold mining operations throughout the world from Northern Ireland, Ghana, Chile, and Argentina (Baraniuk 2020; Haslam 2018; Hausermann, Adomako and Robles 2020). </p><p>An influx of mining activity, driven in part by Chinese miners in Ghana has been linked to several social and health outcomes and has contributed to geopolitical tensions between Ghana and China (Hausermann, Adomako and Robles 2020).</p><p>Illegal and artisanal mining has been reported as contributing to negative social and health outcomes for both local populations and illegal miners (Jonkman and De Theije 2022; Woon and Dodds 2021, p. 353; Hausermann, Adomako and Robles 2020, p. 66).</p><p></p>|

## **5. Analysis**

