---
title: C. Cadmium
description: Critical Minerals in detail  Critical Minerals in detail  C. Cadmium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "C. Cadmium", "cadmium"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.004.png)

Analysis by UNEP. All rights reserved.
## **2. Geographical Data**
![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.005.png)

Data source: USGS 2022, p. 43. Analysis by UNEP. All rights reserved.
## **3. Production and Processes**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Cadmium<br>(<sup>48</sup> Cd)</b>|The availability of cadmium is highly dependent on the production of zinc (Sengupta 2021, pp. 137-138). Secondary cadmium is also recovered from spent nickel cadmium (NiCd) batteries (USGS 2022, 42). Cadmium can also be recovered from copper-cadmium alloy scrap, certain complex nonferrous alloy scrap, cadmium-containing dust from electric arc furnaces, and cadmium telluride (CdTe) solar panels (USGS 2002, 42).||

## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>In the United States, 100% of the cadmium produced comes from zinc mines, meaning that the environmental impacts of mining zinc apply to the production of cadmium (Sengupta 2021, pp. 137-138). Zinc production is known to lead to severe soil contamination of lead, zinc and other associated metals such as cadmium, arsenic and nickel (Sengupta 2021, p. 141). Lead-zinc smelters in south Poland have contributed to high levels of contaminated land including soil levels of cadmium and lead being 10x the proposed limits (Sengupta 2021, p. 141). Plants in this region also show elevated signs of these contaminants (Sengupta 2021, p. 142).</p><p>As much 60% of the total cadmium input into the air comes from smelting and refining (Sengupta 2021, p. 138). </p><p>**In the area surrounding an active Chinese lead-zinc mine it was found that the cadmium concentrations most soil and rice samples exceeded the state proscribed amount (Du *et al.* 2020, p.1).**</p>|<p>Exposure to cadmium is linked to the Itai-Itai disease, which is characterized by osteomalacia, severe bone pain and is associated with renal tubular dysfunction (Nishijo *et al.* 2017, p. 1). Those affected are mainly women residing in rice farming areas irrigated by the contaminated Jinzu River in Japan (Nishijo *et al.* 2017, p. 1). </p><p>Food consumption is the primary source of exposure to cadmium for humans (Baba *et al*. 2013, p. 1228).</p><p>A study found that the causes of death of patients with Itai-Itai disease was attributable to pneumonia and Gastro-intestinal disease (Nishijo *et al.* 2017, pp. 4,7). </p><p>Liver fibrosis has been found to be observed at a significantly higher rate in patients with Itai-Itai disease compared to the control group (Baba *et al*. 2013, p. 1228).</p><p>Cadmium exposure has been linked to several other negative health outcomes in addition to bone and kidney effects including; diabetes, hypertension, myocardial infarction, diminished lung function, age-related macular degeneration and cancer mortality (Satarug *et al.* 2010, p. 182). </p><p>Results showing that residents and farmers living around an active lead-zinc mine had a greater exposure to cadmium suggest that farming and mining practices should not co-exist (Du *et al.* 2020, p. 270). </p><p>Considering that cadmium exposure is greatest through food intake, developing technologies to reduce the availability of cadmium for plant uptake is important for agricultural areas enriched with cadmium due to lead-zinc mining and smelting. In the Guizhou province in China, an area with high cadmium soil levels due in part to historic zinc smelting, it was found that the application of calcareous materials was more applicable in the remediation of cadmium contaminated soils in the area of study (Gong *et al.* 2022, pp. 1-3).</p>||

## **5. Analysis**


