---
title: Q. Platinum Group Minerals [PGMs]
description: Critical Minerals in detail  Q. Platinum Group Minerals [PGMs]
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "Q. Platinum Group Minerals [PGMs]", "platinum_group_minerals"]

---

## **1. Treatment by Critical Minerals Lists**

![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.032.png)

Analysis by UNEP. All rights reserved.

## **2. Geographical Data**

![Graphical user interface, chart, application, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.033.jpeg)

Data source: USGS 2022, pp. 18, 127. Analysis by UNEP. All rights reserved.

## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|**Platinum Group Metals (PGMs)**|<p>By-product of nickel mining. Few dedicated mines. (Canada Action 2021).</p><p>PGMs include palladium, platinum, iridium, osmium, rhodium, and ruthenium.</p>|Beneficiation with flotation followed by smelting (Anglo American n.d.).|

## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>High electricity usage (Bossi and Gediga 2017).</p><p>Power consumption during mining and ore beneficiation have been identified as the major impact of the production of PGMs on the environment (Bossi and Gediga 2017).</p><p>The environment is also impacted from the smelting and refining of PGMs (Bossi and Gediga 2017).</p><p>The risks to the ecosystem from mining PGMs and associated minerals are mainly those related to acid mine drainage, which primarily affects aquatic environments (Zientek et al. n.d.).</p>|<p>PGM’s affect the growth, development, and physiology on humans, plants, and animals (Bossi and Gediga 2017). </p><p>The metallic forms of PGMs are generally considered to be inert – broader human health effects of PGMs appear to be limited because of the low concentrations of PGMs in the environment. Health hazards specifically related to PGMs affect only individuals who are occupationally exposed to manmade PGM compounds. (Zientek *et al.* n.d.).</p>|Given the many adverse effects that mining communities have faced—losing land to mining operations, facing environmental harm, pressure from labor strikes, and mining-related illnesses—without being fully compensated for them, there has been increasing pressure for a transformation of the sector over the last two decades (Baskaran 2021).|

## **5. Analysis**

According to the USGS, world resources of PGMs are estimated to be in excess of 100 million kg, with the largest reserves being in the Bushveld Complex in South Africa (USGS 2022, p. 127). In fact, PGMs are highly concentrated geographically: 2 countries are responsible for 77% of global Palladium production (South Africa and Russia), while South Africa is responsible for 72% of all Platinum production and Russia another 11%. Even more concerningly: South Africa holds 90% of PGM reserves, with Russia holding a further 6%. This makes PGMs highly geographically concentrated. PGMs play a crucial role in hydrogen technology, which, in turn, is key for decarbonizing airplane fuel.
