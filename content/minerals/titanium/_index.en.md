---
title: W. Titanium
description: Critical Minerals in detail  W. Titanium
categories: mining
tags: ["minerals"]
downloadBtn: true
# search keywords
keywords: ["minerals", "mining", "Critical Minerals in detail", "Annex 1", "W. Titanium", "titanium"]

---

## **1. Treatment by Critical Minerals Lists**
![](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.043.png)
Analysis by UNEP. All rights reserved.

## **2. Geographical Data**

![Chart, pie chart Description automatically generated](../../images/minerals/Aspose.Words.cc7a789f-d0da-4c6c-a580-0a6e98189cfe.044.jpeg)

Data source: USGS 2022, p. 18, 179. Analysis by UNEP. All rights reserved.

## **3. Production and Processing**

|**Mineral**|**Extraction**|**Processing**|
| - | :- | :- |
|<b>Titanium <br>(<sup>22</sup> Ti)</b>|<p>Typically exists in chemical combination with either oxygen or iron.</p><p>Heavy mineral sands to rocks.</p><p>Mined using suction dredging. (Perks et al. 2019)</p>|Sands are screened and concentrated using gravity spirals (Tetreault 2019, p. 52).|
## **4. Environmental, Social and Governance Impacts**

|**Environmental challenges**|**Social challenges**|**Governance challenges**|
| :- | - | :- |
|<p>**Mining of titanium oxide causes ground water pollution, environmental damage (coastal damage and deforestation), and omissions associated with transportation (Farjana et al. 2018, p. 20).**</p><p>**There are increased chances of radiation hazards associated with mineral-sand loading and storage facilities (Farjana et al. 2018, p. 20).**</p><p>**If mined using mineral sands, ground water systems are contaminated through radionuclide emissions. This process involves removing topsoil, which may be replaced by tailings thus leading to deforestation and unhealthy topsoil (Farjana et al. 2018, p. 20).**</p>|<p>TiO<sub>2</sub> is a group 2b carcinogen (Skocaj 2011, pp. 227-247).</p><p>Lack of fair compensation (Abuya 2016).</p><p>Instances of human rights abuses (acts of violence committed against protestors opposing mining practices) (Abuya 2016).</p><p>Contamination of drinking water has severe impacts on rural communities (De Jong, Sauerwein and Wouters 2021, p. 83).</p><p>Rio Tinto, a titanium mining corporation has been associated with the negative handling of indigenous land rights (De Jong, Sauerwein and Wouters 2021, p. 86).</p>|<p>Lack of proper legal and regulatory framework to govern Environmental Performance Deposit Bonds (EPDBs) regarding titanium mining in Kenya (Omedo *et al.* 2022, p. 1).</p><p>Corruption (police force/lack of regulations) leading to human rights violations associated with protestors (Abuya 2016).</p>|
## **5. Analysis**
High geographical concentration: three countries account for 68% of titanium reserves (China, Australia and India), while three countries have are also responsible for 60% of global production (China, South Africa and Mozambique).

