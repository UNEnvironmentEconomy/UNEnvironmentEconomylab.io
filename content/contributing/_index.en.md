---
title: Contributing
icon: "ti-comments"
description: Critical Minerals in detail Information for contributors to the UNICEF Inventory theme or UNICEF Toolkits using this theme.
type: docs
weight: 40

---
