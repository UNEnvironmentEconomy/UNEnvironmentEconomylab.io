---
title: Infographic – 1.D Market Concentration
icon: ti-hummer
description: Graphics to show market concentration of key supply chains
type: docs
weight: 34
downloadBtn: true
# search keywords
keywords: ["market concentration", "supply chains"]

---
