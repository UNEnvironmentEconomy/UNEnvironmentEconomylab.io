---
title: Infographic – 1.A Renewable Technologies
icon: ti-hummer
description: page infographic showing the various uses of critical minerals in renewable technologies
type: docs
weight: 31
downloadBtn: true
# search keywords
keywords: ["renewable technologies", "renewable", "technologies"]

---

![Chinas-Rare-Earth-Exports](https://www.visualcapitalist.com/wp-content/uploads/2021/01/Tracking-Chinas-Rare-Earth-Exports-2-1.jpg)