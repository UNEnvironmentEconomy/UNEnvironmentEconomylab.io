---
title: Reserves
icon: ti-hummer
description: Reserves
type: docs
weight: 44
downloadBtn: true
# search keywords
keywords: ["reserves",]

---

<iframe width="600" height="450" src="https://lookerstudio.google.com/embed/reporting/35c643cd-1dab-48ca-95c9-4e2b19b8875c/page/WShKD" frameborder="0" style="border:0" allowfullscreen></iframe>

Source: U.S. Geological Survey, 2023, Data release for mineral commodity summaries 2023: U.S. Geological Survey data release, https://doi.org/10.5066/P9WCYUI6.