---
title: Infographic – 1.B Demand Prediction
icon: ti-hummer
description: graphs of % increases in demand predicted over current rates by 2050 as a result of cumulative impact of energy transition and digitisation
type: docs
weight: 32
downloadBtn: true
# search keywords
keywords: ["renewable technologies", "renewable", "technologies", "digitisation", "energy transition", "demand"]

---
