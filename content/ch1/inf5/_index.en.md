---
title: Infographic – 1.E Ore Quality
icon: ti-hummer
description: Graphic to show trend of declining ore quality across key critical minerals
type: docs
weight: 35
downloadBtn: true
# search keywords
keywords: ["declining ore quality", "critical minerals"]

---
