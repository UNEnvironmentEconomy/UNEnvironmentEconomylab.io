---
title: Infographic – 1.C Global Reserves
icon: ti-hummer
description: visual representation of global reserves, major processing facilities and trade routes for one or more of the critical minerals
type: docs
weight: 33
downloadBtn: true
# search keywords
keywords: ["processing", "trade routes", "reserves"]

---

![shipmap](https://www.shipmap.org/images/colored_preview.jpg)

<iframe src="//www.shipmap.org" style="width: 100%; height: 600px; border: 0" frameborder="0"></iframe><div style="width: 100%; font-size: 10px; margin-top: 4px">Created by <a target="_top" href="https://www.kiln.digital/">London-based data visualisation studio Kiln</a> and the <a target="_top" href="http://www.bartlett.ucl.ac.uk/energy">UCL Energy Institute</a></div>


<iframe width="600" height="450" src="https://lookerstudio.google.com/embed/reporting/35c643cd-1dab-48ca-95c9-4e2b19b8875c/page/WShKD" frameborder="0" style="border:0" allowfullscreen></iframe>

Source: U.S. Geological Survey, 2023, Data release for mineral commodity summaries 2023: U.S. Geological Survey data release, https://doi.org/10.5066/P9WCYUI6.