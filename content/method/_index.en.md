---
title: Annex 2
icon: ti-ruler-pencil
description: Critical Minerals in detail Annex 2-- Methodological annex on selection of critical minerals 
type: docs
weight: 30

---

# I. Metals and Minerals Key to the Energy Transition

## A. Introduction
Various Critical Mineral Lists stipulate a range of key minerals and metals that are core to the energy transition or otherwise strategic in nature. These include the US List of Critical Minerals (United States Department of the Interior 2022), the EU Critical Raw Materials Act in parallel with the EU Net Zero Industry Act, the Australian Critical Minerals Strategy (Australia Department of Industry Trade and Resources 2022), and the Canadian Critical Minerals List (Natural Resources Canada 2021). These lists are frequently accompanied by corporate financial incentives, intended to strengthen national or regional supply sufficiency. In the case of the US it is the *Inflation Reduction Act of 2022*; in Canada there is the critical minerals exploration tax credit (CMETC) that will offer a 30 per cent tax credit to qualifying exploration expenses (Suarez 2022).

The US Critical Minerals List (CML) came into being pursuant to Presidential Executive Order 13817<a name="_ref116125989"></a> (EO 13817 2117) and defines a “critical mineral” as one that meets three criteria: (1) it must be a non-fuel mineral or mineral material that is deemed to be essential to the economic and national security of the United States; (2) its supply chain must be vulnerable to disruption; and (3) it must serve an essential function in product manufacture, so that its absence would entail significant consequences for the US economy or national security (EO 13817 2117, s. 2). Such supply chain disruptions could arise for a variety of reasons, including “natural disasters, labor strife, trade disputes, resource nationalism, and conflict.” (Hammarstrom *et al.* 2022, p. ii). It bears pointing out that in determining criticality, factors beyond net import reliance were considered, to include also indirect trade reliance (obscure country of origin), embedded trade reliance (commodity forms part of an imported product), as well as foreign ownership of mineral assets and processing facilities (Hammarstrom *et al.* 2022, p.1).

## B. A Comparison of Key Critical Minerals Lists

8 Key Critical Mineral Lists were selected for comparison: those of the IEA (IEA 2022a), the World Bank (WB 2020a), the USA (USGA 2022), the EU (2022), Canada (2021), Australia (2022), as well as two recent studies commissioned by the EU: Eurométaux (2022) and Systemiq (2022). 

In order to make possible a line by line comparison of the minerals listed, the majority approach was followed by grouping platinum group minerals (PGMs) and rare earth elements (REEs) respectively. There are 6 PGMs (palladium, platinum, iridium, osmium, rhodium, and ruthenium) and 17 REEs (the lanthanide group of the periodic table, with the frequent inclusion of Yttrium and Scandium). 

In the next step, a line-by-line comparison was conducted of the elements that appeared on the 8 lists in question, in order to determine how universally critical specific minerals were deemed to be. This is because most critical mineral lists also contain minerals of strategic concern, and strategic concerns are frequently regional in nature.  With the PGMs and the REEs compressed, this made for a range of 47 critical elements across the 8 lists studied.

![A screenshot of a computer Description automatically generated with medium confidence](./../images/minerals/Aspose.Words.cdafb522-bab2-4a4d-b169-6b676392f3a8.001.png)

Detailed comparison of critical mineral lists. Copyright UNEP. All rights reserved.

The comparison delivered some interesting results:

- **Tier 1:** The only two minerals to appear across all eight lists, were lithium and REEs. Lithium is crucial for Lithium ion battery technology (everything from smart phones to electric vehicles); REEs are essential components of wind turbines and other smart electronics.
- **Tier 2:** Chromium, Cobalt, Indium, PGMs and Vanadium appeared across 7 lists. Cobalt and Vanadium are battery metals. Chromium is used to harden steel, which is crucial for wind turbines. Indium is a vital component of solar panels. PGMs play a key role in hydrogen conversion technologies.
- **Tier 3:** Aluminium, Gallium, Germanium, Graphite, Manganese and Nickel made 6 lists. Aluminium is crucial for electricity wiring; Gallium and Germanium are used in solar panels; Graphite and Nickel are battery minerals. Manganese is a transition metal used in stainless steel.
- **Tier 4:** Magnesium, Niobium, Silicon, Tantalum, Titanium, Tungsten and Zinc form part of 5 lists. Magnesium, Niobium, Tantalum, Titanium, Tungsten and Zinc are important alloys and superalloys. Silicon is essential to solar panels.

The third step was to obtain a picture of the actual status of the most commonly identified elements in relation to their current consumption levels (i.e., as at 2023).

![Chart, bubble chart Description automatically generated](./../images/minerals/Aspose.Words.cdafb522-bab2-4a4d-b169-6b676392f3a8.002.png)

Status of the critical elements at actual consumption levels. Data: USGS 2022. Analysis by UNEP. Copyright UNEP. All rights reserved.

- The two **Tier 1** minerals, Lithium and REEs, both already present supply concerns: Lithium has limited availability, while the REEs represent the full range of the spectrum, meaning that they range from serious threat of availability to plentiful supply, dependent on the element in question. It is beyond the scope of this study to delve further into this aspect.
- The five **Tier 2** minerals, Chromium, Cobalt, Indium, PGMs and Vanadium range from limited availability (Vanadium) to rising threat (Chromium; Cobalt, PGM) to serious threat (Indium).
- The six **Tier 3** minerals, Aluminium, Gallium, Germanium, Graphite, Manganese and Nickel range from plentiful supply (Aluminium) to limited availability (Manganese, Nickel) to serious threat (Gallium, Germanium, Graphite).
- The seven **Tier 4** minerals, Magnesium, Niobium, Silicon, Tantalum, Titanium, Tungsten and Zinc, range from plentiful supply (Silicon, Titanium, Zinc) to limited availability (Magnesium, Niobium) to serious threat (Tungsten).

Another way of envisaging this, is by group: of the 47 minerals considered, 11 (23%) are in plentiful supply; 18 (38%) have limited availability; 5 (11%) are under rising threat; 12 (26%) are under serious threat. REE (2%) spans across the entire range because of its complex constituent parts.

However, the availability of these minerals and metals to the end consumers thereof depends on more than the reserves and resources that are in the ground. For instance, bottlenecks may arise where the production or refining or a mineral is particularly concentrated in a given geographical area. It is therefore important to take a closer look.

## C. Core List of Minerals and Metals Key to the Sustainable Energy Transition

### **1. Methodology for selection of key minerals**

A list of 47 minerals is unwieldy for detailed study. As previously mentioned, it contains minerals of a strategic nature, in addition to some that are associated with nuclear energy. This report has elected to focus on a list of 26 elements based on IEA list (2022a) read with that of Eurométaux (2022). Ultimately, the criterion for inclusion on the list is whether the mineral is key to the energy transition for its use in  transportation or energy generation, transmission or storage technologies.

The underlying rationale of the study that follows is qualitative rather than quantitative. In other words, this study seeks not to identify the most important metals that will be impacted by the energy transition – these are commonly agreed to be cobalt, copper, lithium and nickel (IMF 2021, p. 6 with reference to WB 2020a and IEA 2022a’s 2021 version)–,  but asks the following questions: 

- Which minerals and metals will be needed to make the sustainable energy transition a reality?
- How are they produced and processed?
- What are the main geographical areas where they are located and refined? How concentrated is their production or processing, geographically speaking?
- Are there environmental, social and/or governance challenges specifically associated with such production and/or processing?
- Are there easy substitutes for them?
- What do we know about their availability going forward, notably in the context of the sustainable energy transition’s increased demand for minerals?

This methodology can be distinguished from that followed by the various ‘critical minerals’ lists that are in circulation. What is ‘critical’ depends in any given list on the definition adopted by the compilers of the list. For instance, the US list of 35 minerals or mineral groups deemed critical was finalized in May 2018 (83 FR 23295-23296) and is subject to revision every three years (Energy Act of 2020). Its 2021 revision resulted in the substitution of some minerals and the addition of others, so that the list now contains 50 individual mineral commodities (US Geological Survey [USGS] 2022, p. 17). One notable change is that the updated list ungroups the rare earth elements (REEs) and the platinum group metals (PGMs). This study – like the majority of critical mineral lists out there– has elected to group them.

![A computer screen capture Description automatically generated with medium confidence](../../images/minerals/Aspose.Words.cdafb522-bab2-4a4d-b169-6b676392f3a8.003.png)

Source: UNEP analysis based on IEA (2022a) and Eurométaux (2022). Analysis by UNEP. All rights reserved.

**Table 1  Minerals and metals key to the sustainable energy transition**

