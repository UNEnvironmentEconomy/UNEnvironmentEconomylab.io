---
title: "Frequently Asked Questions"
draft: false
---

{{< faq "How can I access the digital annexes of the UN report on the GitLab-Pages hosted micro-website?" >}}
To access the digital annexes of the UN report, please visit the following GitLab URL at [website-url](https://examplesite.com). On the homepage, you will find a dedicated section or link specifically for the digital annexes. Click on the link, and it will take you to a page where you can browse and download the  annexes related to the report. The annexes are available in various file  formats, such as PDF, Excel, or Word, depending on the content.

{{</ faq >}}

{{< faq "Are the digital annexes searchable or indexed for easy navigation?" >}}
Yes, the digital annexes of the UNEP report, Responsible Mining for the Transition, are searchable and indexed for easy navigation. We have implemented a search functionality that allows you to enter keywords or phrases related to the content you are looking for. Simply type your query into the [search bar](https://examplesite.com), and the website will display relevant results from the digital annexes.

{{</ faq >}}
